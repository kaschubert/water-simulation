#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
#endregion

using MARSHAL = System.Runtime.InteropServices.Marshal;

////////////////////////////////////////////////////////////////////////////////////////////

struct WATER_VERTEX
{
    public Vector2 p; //Position
}

////////////////////////////////////////////////////////////////////////////////////////////

class CWater
{
	CRenderEngine m_pRenderEngine;

	CEffect		  m_pEffect;		//Shader of the water
	CVertexBuffer m_pVB;			//Water surface vertex buffer
    CIndexBuffer  m_pIB;			//Water surface index buffer
    int           m_nVertices;		//Vertex count of the water surface
    int           m_nPrimitives;	//Primitive count of the water surface

	CTexture	  m_SkyCubeMap;
	CTexture	  m_CausticMap;
	CTexture	  m_FoamMap;
	CTexture	  m_WaveMap;

    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    public CWater(CRenderEngine pRenderEngine)
    {
		m_pRenderEngine = pRenderEngine;

		//Load the water shader
		 m_pEffect = m_pRenderEngine.GetShaderManager().GetEffect("media//shader//water.fx");

		//Load textures
		m_SkyCubeMap = m_pRenderEngine.GetTextureManager().GetTexture("media//textures//SkyCubeMap.dds");
		m_CausticMap = m_pRenderEngine.GetTextureManager().GetTexture("media//textures//CausticMap.dds");
		m_FoamMap	 = m_pRenderEngine.GetTextureManager().GetTexture("media//textures//FoamMap.dds");
		m_WaveMap	 = m_pRenderEngine.GetTextureManager().GetTexture("media//textures//WaveMap.dds");		

		//Create the water buffers
		int Size = 256; //Number of tiles/quads per row

		//Compute vertex and primitive count of the complete hair
		m_nVertices   = (Size + 1) * (Size + 1);
        m_nPrimitives = Size * Size * 2;

		//Create temporary vertex and index buffer
        WATER_VERTEX[] pDataVB = new WATER_VERTEX[m_nVertices];
        int[]          pDataIB = new int[m_nPrimitives * 3];

		m_pVB = m_pRenderEngine.GetBufferManager().GetVertexBuffer(MARSHAL.SizeOf(typeof(WATER_VERTEX)), m_nVertices);
        m_pIB = m_pRenderEngine.GetBufferManager().GetIndexBuffer(4, m_nPrimitives * 3);

		//Fill the vertices
		for (int z = 0; z < Size + 1; ++z)
		{
			for (int x = 0; x < Size + 1; ++x)
			{
				pDataVB[x + z * (Size + 1)].p = new Vector2((float)x /(float) Size, (float)z /(float) Size);
			}
		}

		//Fill the indices
		for (int z = 0; z < Size; ++z)
		{
			for (int x = 0; x < Size; ++x)
			{
				//For every tile create two triangles
				int Index  = (x + z * Size) * 6;
				int Vertex =  x + z * (Size + 1);

				//1. Triangle of the line
                pDataIB[Index + 0] = Vertex + (Size + 1);
                pDataIB[Index + 1] = Vertex + 0;
                pDataIB[Index + 2] = Vertex + (Size + 1) + 1;

                //2. Triangle of the line
                pDataIB[Index + 3] = Vertex + 1;
                pDataIB[Index + 4] = Vertex + (Size + 1) + 1;
                pDataIB[Index + 5] = Vertex + 0;
			}
		}

        //Copy data to the buffers (graphic card)
        m_pVB.SetData<WATER_VERTEX>(pDataVB);
        m_pIB.SetData<int>(pDataIB);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    ~CWater()
    {
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////

    public void Update()
    {
      
    }

	////////////////////////////////////////////////////////////////////////////////////////////

	public void Render()
    {
        if (m_pRenderEngine == null || m_pEffect == null)
        {
            return;
        }

        //Set Vertex- and Indexbuffer
        m_pVB.SetBuffer(0);
        m_pIB.SetBuffer();

        //Set technique of the effect
        int nPasses = m_pEffect.BeginEffect(0);

        if (nPasses == 0)
        {
            return;
        }

		if (m_pRenderEngine.GetBackbufferCopy() != null)
		{
			m_pEffect.SetTexture(0, m_pRenderEngine.GetBackbufferCopy());
		}

		m_pEffect.SetTexture(1, m_SkyCubeMap);
		m_pEffect.SetTexture(2, m_WaveMap);
		m_pEffect.SetTexture(3, m_FoamMap);
		m_pEffect.SetTexture(4, m_CausticMap);		

        //Run through all passes
        for (int i = 0; i < nPasses; i++)
        {
            m_pEffect.BeginPass(i);

            //Render the complete hair
            m_pRenderEngine.GetGraphicsDevice().DrawIndexedPrimitives(PrimitiveType.TriangleList,
                    0, 0, m_nVertices, 0, m_nPrimitives);

            m_pEffect.EndPass();
        }

		//Reset effect
		m_pEffect.EndEffect();
	}

    ////////////////////////////////////////////////////////////////////////////////////////////
};
