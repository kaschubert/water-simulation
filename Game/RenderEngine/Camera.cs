#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
#endregion

////////////////////////////////////////////////////////////////////////////////////////////

class CCamera
{
    public Vector3 m_Direction = new Vector3(0.0f,  0.0f,  0.0f);
    public Vector3 m_Position  = new Vector3(0.0f, 96.0f, 96.0f);
    public Vector3 m_WorldUp   = new Vector3(0.0f,  1.0f,  0.0f);
    public Vector3 m_LookAt    = new Vector3(0.0f,  5.0f,  0.0f);

    public float m_FOV            = (float)Math.PI / 2.0f;
    public float m_ApsectRatio    = 4.0f / 3.0f;
    public float m_NearClipPlane  = 0.5f;
    public float m_FarClipPlane   = 1000.0f;

    public Matrix m_matView;
    public Matrix m_matProj;
    public Matrix m_matViewProj;


    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    public CCamera()
    {
        Update();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    ~CCamera()
    {
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////

    public void Update()
    {
        m_Direction = Vector3.Normalize(m_LookAt - m_Position);

        m_matView = Matrix.CreateLookAt(m_Position, m_LookAt, m_WorldUp);
        m_matProj = Matrix.CreatePerspectiveFieldOfView(m_FOV, m_ApsectRatio, 
                                                        m_NearClipPlane, m_FarClipPlane);
        m_matViewProj = Matrix.Multiply(m_matView, m_matProj);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void AddToCamera(float Strafe, float UpDown, float ForBack)
    {
	    Vector3 Direction = m_Direction;
	    Vector3 Ortho = new Vector3(-Direction.Z, 0.0f, Direction.X);
				Ortho = Ortho * Strafe;

	    Direction = Direction * ForBack;

	    Vector3 VecUpDown = new Vector3(0.0f, 1.0f * UpDown, 0.0f);

        m_LookAt   = m_LookAt   + Direction + VecUpDown + Ortho;
        m_Position = m_Position + Direction + VecUpDown + Ortho;	

	    Update();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void RotateAroundPosition(float x, float y)
    {
	    Matrix matRotate;

	    //Rotation Left/Right
	    m_LookAt  = m_LookAt - m_Position;
        matRotate = Matrix.CreateFromAxisAngle(new Vector3(0.0f, 1.0f, 0.0f), x);
        m_LookAt  = Vector3.Transform(m_LookAt, matRotate);
	    m_LookAt  = m_LookAt + m_Position;

	    m_Direction = Vector3.Normalize(m_LookAt - m_Position);
   
	    //Rotation Up/Down
	    Vector3 Ortho = new Vector3(-m_Direction.Z, 0.0f, m_Direction.X);

	    m_LookAt  = m_LookAt - m_Position;
        matRotate = Matrix.CreateFromAxisAngle(Ortho, y);
        m_LookAt  = Vector3.Transform(m_LookAt, matRotate);
	    m_LookAt  = m_LookAt + m_Position;
        
        Update();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

};
