#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
#endregion


////////////////////////////////////////////////////////////////////////////////////////////

class CShadowMap
{
	CRenderEngine	m_pRenderEngine;
	CRendertarget	m_pTexShadowMap;
    CCamera			m_pLightCamera;
	Matrix			m_matViewProjProjected;

    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    public CShadowMap(CRenderEngine pRenderEngine)
    {
		m_pRenderEngine = pRenderEngine;

        m_pLightCamera = new CCamera();

        m_pLightCamera.m_FOV = (float)Math.PI / 2.0f;
        m_pLightCamera.m_Position = m_pRenderEngine.GetLight().m_Position;
        m_pLightCamera.m_LookAt   = new Vector3(0.0f, 5.0f, 0.0f);
        m_pLightCamera.m_NearClipPlane = 1.0f;
        m_pLightCamera.m_FarClipPlane  = 1000.0f;
        m_pLightCamera.Update();

        m_pTexShadowMap = m_pRenderEngine.GetTextureManager().GetRendertarget(SurfaceFormat.Single, 2048, 2048, true);

		Update();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    ~CShadowMap()
    {
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////

    public void Update()
    {
        m_pLightCamera.m_Position = m_pRenderEngine.GetLight().m_Position;
        m_pLightCamera.m_LookAt = new Vector3(30.0f, 0.0f, 30.0f);
        m_pLightCamera.Update();

        if (m_pTexShadowMap == null)
        {
            return;
        }

        float fOffsetX = 0.5f + (0.5f / (float)m_pTexShadowMap.Width); //Half Texel Offset
        float fOffsetY = 0.5f + (0.5f / (float)m_pTexShadowMap.Height);

		Matrix matProjected = new Matrix
		(
			0.5f,     0.0f,      0.0f,  0.0f,
			0.0f,    -0.5f,      0.0f,  0.0f,
			0.0f,     0.0f,      1.0f,  0.0f,
			fOffsetX, fOffsetY,  0.0f,  1.0f
		);

		m_matViewProjProjected = m_pLightCamera.m_matViewProj * matProjected;
    }

	////////////////////////////////////////////////////////////////////////////////////////////

    public CRendertarget GetShadowMap()
	{
        return m_pTexShadowMap;
	}

    ////////////////////////////////////////////////////////////////////////////////////////////
	
	public bool BeginShadowMap()
	{
        if (m_pTexShadowMap == null)
        {
            return false;
        }

        if (m_pTexShadowMap.BeginScene() == false)
        {
            return false;
        }

        if (m_pRenderEngine != null && m_pRenderEngine.GetGraphicsDevice() != null)
        {
            m_pRenderEngine.GetGraphicsDevice().Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.White, 1.0f, 0);
        }

		return true;
	}

    ////////////////////////////////////////////////////////////////////////////////////////////

	public void EndShadowMap()
	{
        if (m_pTexShadowMap == null)
        {
            return;
        }

        m_pTexShadowMap.EndScene();
	}

    ////////////////////////////////////////////////////////////////////////////////////////////

	public Matrix GetMatrixVP()
	{
		return m_pLightCamera.m_matViewProj;
	}

    ////////////////////////////////////////////////////////////////////////////////////////////

	public Matrix GetMatrixVPP()
	{
		return m_matViewProjProjected;
	}

    ////////////////////////////////////////////////////////////////////////////////////////////
};
