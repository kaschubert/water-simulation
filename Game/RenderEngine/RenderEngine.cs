#region Using Statements
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Game;
#endregion

//RenderFlag
enum RF
{
    NORMAL  = 0x00000001,
    SHADOW  = 0x00000002,

	ALL = RF.NORMAL | RF.SHADOW,
}

class CRenderEngine
{
    //Device
    GraphicsDeviceManager m_pGraphicsManager;
    GraphicsDevice		  m_pGraphicsDevice;

    //Manager
    CTextureManager m_pTextureManager;
    CShaderManager  m_pShaderManager;
    CBufferManager  m_pBufferManager;

    //Attributes
    float           m_fTime;
    float           m_fElapsedTime;

	CRendertarget	m_pBackbufferCopy;
    CCamera         m_pCamera;
	CShadowMap      m_pShadowMap;
    CWater	        m_pWater;
	CLight	        m_Light;

    //Test objects
    List<CModel>    m_Models;

    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    public float GetTime()        { return m_fTime;        }
    public float GetElapsedTime() { return m_fElapsedTime; }

    public CCamera               GetCamera()         { return m_pCamera;          }
    public CShadowMap	         GetShadowMap()	     { return m_pShadowMap;       }
    public CRendertarget		 GetBackbufferCopy() { return m_pBackbufferCopy;  }
	public CWater		         GetWater()			 { return m_pWater;			  }
	public CLight                GetLight()          { return m_Light;            }
    public CShaderManager        GetShaderManager()  { return m_pShaderManager;   }
    public CTextureManager       GetTextureManager() { return m_pTextureManager;  }
    public CBufferManager        GetBufferManager()  { return m_pBufferManager;   }
    public GraphicsDevice        GetGraphicsDevice() { return m_pGraphicsDevice;  }
    public GraphicsDeviceManager GetGraphicsManager(){ return m_pGraphicsManager; }

    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    public CRenderEngine(GraphicsDeviceManager pGraphicsManager)
    {
        m_fTime            = 0.0f;
        m_fElapsedTime     = 0.0f;
        m_pGraphicsManager = pGraphicsManager;
        m_pGraphicsDevice  = m_pGraphicsManager.GraphicsDevice;

        //Attributes
        m_pCamera          = new CCamera();

        //Init Manager
        m_pShaderManager  = new CShaderManager(this);
        m_pTextureManager = new CTextureManager(this);
        m_pBufferManager  = new CBufferManager(this);

		m_pBackbufferCopy = m_pTextureManager.GetRendertarget(SurfaceFormat.Vector4,
			(uint)m_pGraphicsManager.PreferredBackBufferWidth, (uint)m_pGraphicsManager.PreferredBackBufferHeight, false);

		//Init Light and Shadows
        m_Light = new CLight(new Vector3(50.0f, 30.0f, 0.0f),//80.0f, 25.0f, 40.0f
						     new Vector3(0.30f, 0.30f, 0.30f),
                             new Vector3(1.00f, 0.92f, 0.95f) * 1,//new Vector3(1.0f, 0.8f,  0.8f),
							 new Vector3(0.00f, 0.00f, 0.00f));

        m_pShadowMap = new CShadowMap(this); //Create after Light
		m_pWater	 = new CWater(this);

        m_Models = new List<CModel>();

        SetDefaultStates();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    ~CRenderEngine()
    {
        //Release Manager
    }
    ////////////////////////////////////////////////////////////////////////////////////////////

    public void LoadContent()
    {
        m_Models.Add(new CModel(this, "content//Sky", "media//Shader//sky.fx", "media//textures//SkyCubeMap.dds"));
		m_Models.Add(new CModel(this, "content//Scene", "media//Shader//mesh.fx", "content//Scene_Norm.png"));
        //m_Models.Add(new CModel(this, "content//Pyramid", "media//Shader//Pyramid.fx"));
        
		//Don't draw sky into the shadow map
		m_Models[0].SetRenderFlags((int)RF.NORMAL);

        float Scale = 1.0f;

        for (int i = 0; i < m_Models.Count; i++)
        {
            if (m_Models[i] == null)
            {
                //Erase models which can't be loaded
                m_Models.RemoveAt(i);

                i = i - 1;
            }
            else
            {
                m_Models[i].SetScale(new Vector3(Scale, Scale, Scale));
                m_Models[i].SetTranslation(new Vector3(0.0f, 0.0f, 0.0f));
                m_Models[i].SetRotation(new Vector3(0.0f, 0.0f, 0.0f)); //(float)Math.PI / 2.0f
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void Update(float fElapsedTime)
    {
        m_fTime       += fElapsedTime;
        m_fElapsedTime = fElapsedTime;

        m_Light.m_Position = new Vector3(50.0f, 30.0f, 0.0f);
        m_Light.m_Position = Vector3.Transform(m_Light.m_Position, Matrix.CreateRotationY(m_fTime * 0.4f));

        if (m_pShadowMap != null)
        {
            m_pShadowMap.Update();
        }

		if (m_pWater != null)
		{
			m_pWater.Update();
		}

        if (m_pShaderManager != null)
        {
            m_pShaderManager.UpdateParameters();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void Render()
    {
        if (m_pShadowMap != null)
		{
            if (m_pShadowMap.BeginShadowMap())
			{
				RenderScene(RF.SHADOW);

                m_pShadowMap.EndShadowMap();
			}
		}

		if (m_pBackbufferCopy.BeginScene(1) == false)
        {
			return;
        }

		m_pGraphicsDevice.Clear(ClearOptions.Target|ClearOptions.DepthBuffer, new Color(0, 0, 0, 0), 1.0f, 0);//|ClearOptions.Stencil

		RenderScene(RF.NORMAL);

		m_pBackbufferCopy.EndScene();
        
		if (m_pWater != null)
		{
			m_pWater.Render();
		}
    }

	////////////////////////////////////////////////////////////////////////////////////////////

    public void RenderScene(int iRenderflag)
    {
        RenderScene((RF)iRenderflag);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void RenderScene(RF Renderflag)
    {
        switch (Renderflag)
        {
            case RF.NORMAL:
                for (int i = 0; i < m_Models.Count; i++)
                {
                    m_Models[i].Render(Renderflag);
                }
                break;
            case RF.SHADOW:
                for (int i = 0; i < m_Models.Count; i++)
                {
                    m_Models[i].Render(Renderflag);
                }
				break;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    void SetDefaultStates()
    {
        //Init SamplerStates
        for (int i = 0; i < 16; i++)
        {
            m_pGraphicsDevice.SamplerStates[i].MinFilter = TextureFilter.Anisotropic;
            m_pGraphicsDevice.SamplerStates[i].MagFilter = TextureFilter.Anisotropic;
            m_pGraphicsDevice.SamplerStates[i].MipFilter = TextureFilter.Linear;
        }

        //Init RenderStates
        m_pGraphicsDevice.RenderState.AlphaTestEnable       = true;
        m_pGraphicsDevice.RenderState.ReferenceAlpha        = 0;
        m_pGraphicsDevice.RenderState.AlphaSourceBlend      = Blend.SourceAlpha;
        m_pGraphicsDevice.RenderState.AlphaDestinationBlend = Blend.InverseSourceAlpha;
        m_pGraphicsDevice.RenderState.CullMode              = CullMode.CullCounterClockwiseFace;

		m_pGraphicsDevice.RenderState.DepthBufferEnable      = true;
		m_pGraphicsDevice.RenderState.DepthBufferWriteEnable = true;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void LostDevice()
    {
        //Device is lost (e.g. because of resize or minimized window)        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////

    public void ResetDevice()
    {
        //After the device was lost some unmanaged resources must be reseted
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
};
