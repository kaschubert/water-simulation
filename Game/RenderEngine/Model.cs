#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
#endregion

using MARSHAL = System.Runtime.InteropServices.Marshal;

namespace Game
{
    struct MESH_VERTEX
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Vector2 TexCoord;
        public Vector3 BiTangent;
        public Vector3 Tangent;
    }

    class CModel
    {
        protected CRenderEngine m_pRenderEngine; //Pointer to the renderengine

        protected Model m_Mesh;                  //Real mesh (including vertices, indices,
                                                 //materials, diffuse texture, ...)
        protected CEffect m_pEffect;             //Assigned effect/shader
        protected List<CTexture> m_Textures;     //Array of extra textures (e.g. normal map)

        protected Vector3 m_Position;            //Position of the mesh
        protected Vector3 m_Rotation;            //Rotation of the mesh
        protected Vector3 m_Scale;               //Scaling values of the mesh
        protected Matrix  m_matWorld;            //Combined transformations
        protected Matrix  m_matInvWorld;         //Inverse world matrix
        protected Matrix  m_matNormal;           //Combinded transformations for the normal
        protected Matrix  m_matTexture;          //Transformation from Projection to Texture space

		protected int	  m_RenderFlags;

        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////
        
		public void SetRenderFlags(int RenderFlags)
		{
			m_RenderFlags = RenderFlags;
		}

		////////////////////////////////////////////////////////////////////////////////////////////

        public CModel()
        {
			m_RenderFlags = (int)RF.ALL;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////

        public CModel(CRenderEngine pRenderEngine, string ModelPath, string ShaderPath, params string[] ExtraTexturePaths)
        {
            m_pRenderEngine = pRenderEngine;
            m_RenderFlags = (int)RF.ALL;

            ContentManager CM = Game.m_ContentManager;

            //Load x or fxb file
            m_Mesh = CM.Load<Model>(ModelPath);

            ComputeTangentSpace();

            //Load effect file
            m_pEffect = m_pRenderEngine.GetShaderManager().GetEffect(ShaderPath);

            //Load extra textures
            m_Textures = new List<CTexture>();

            foreach (string ExtraTexturePath in ExtraTexturePaths)
            {
                m_Textures.Add(m_pRenderEngine.GetTextureManager().GetTexture(ExtraTexturePath));
            }

            //Init transformation
            m_Position = new Vector3(0.0f, 0.0f, 0.0f);
            m_Rotation = new Vector3(0.0f, 0.0f, 0.0f);
            m_Scale = new Vector3(1.0f, 1.0f, 1.0f);
            m_matWorld = new Matrix();
        }
     
        ////////////////////////////////////////////////////////////////////////////////////////////

        public void Update()
        {
            //m_vPosition
            Matrix matTranslate = Matrix.CreateTranslation(m_Position);
            Matrix matRotateX   = Matrix.CreateRotationX(m_Rotation.X);
            Matrix matRotateY   = Matrix.CreateRotationY(m_Rotation.Y);
            Matrix matRotateZ   = Matrix.CreateRotationZ(m_Rotation.Z);
            Matrix matRotate    = matRotateX * matRotateY * matRotateZ;
            Matrix matScale     = Matrix.CreateScale(m_Scale);
            Matrix matInvScale  = Matrix.CreateScale(1.0f / m_Scale.X, 1.0f / m_Scale.Y, 1.0f / m_Scale.Z);

            //Combine all transformations
            m_matWorld = matScale * matRotate * matTranslate;

            //Invert world matrix
            m_matInvWorld = Matrix.Invert(m_matWorld);

            //Only rotation and inverse scale are applied to the normal transformation
            m_matNormal = matInvScale * matRotate; //Matrix.Transpose(Matrix.Invert(matScale * matRotate));//
        }

        ////////////////////////////////////////////////////////////////////////////////////////////

        public void SetTranslation(Vector3 vPosition)
        {
            m_Position = vPosition;

            Update();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////

        public void SetRotation(Vector3 vRotation)
        {
            m_Rotation = vRotation;

            Update();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////

        public void SetScale(Vector3 vScale)
        {
            m_Scale = vScale;

            Update();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////

        virtual public void Render(RF RenderFlag)
        {
            if (m_pRenderEngine == null || m_pEffect == null || m_Mesh == null)
            {
                return;
            }

			if ((m_RenderFlags & (int)RenderFlag) == 0)
			{
				return;
			}

            //Compute current technique
            int Technique = 0;

            switch (RenderFlag)
            {
                default:
                case RF.NORMAL:
                    Technique = 0;
                    m_pEffect.SetTexture(1, m_pRenderEngine.GetShadowMap().GetShadowMap()); 
                    break;
                case RF.SHADOW:
                    Technique = 1;
                    break;
            }

            int nPasses = m_pEffect.BeginEffect(Technique);

            if (nPasses == 0)
            {
                return;
            }

            //Set extra textures (maximum 6)
            for (int i = 0; i < Math.Min(m_Textures.Count, 6); i++)
            {
                m_pEffect.SetTexture(2 + i, m_Textures[i]);
            }

            //Set world and inverse world matrix
            m_pEffect.SetParameter("matN", m_matNormal);
            m_pEffect.SetParameter("matW", m_matWorld);
            m_pEffect.SetParameter("matInvW", m_matInvWorld);

            //Set Vertex- and Indexbuffer
            for (int j = 0; j < m_Mesh.Meshes.Count; j++)
            {
                m_pRenderEngine.GetGraphicsDevice().Indices = m_Mesh.Meshes[j].IndexBuffer;

                m_pRenderEngine.GetGraphicsDevice().Vertices[0].SetSource(m_Mesh.Meshes[j].VertexBuffer, 0,
                                                                          m_Mesh.Meshes[j].MeshParts[0].VertexStride);

                //Render different parts of the mesh
                for (int i = 0; i < m_Mesh.Meshes[j].MeshParts.Count; i++)
                {
                    int nVertices      = m_Mesh.Meshes[j].MeshParts[i].NumVertices;
                    int Offset         = m_Mesh.Meshes[j].MeshParts[i].StreamOffset;
                    int StartIndex     = m_Mesh.Meshes[j].MeshParts[i].StartIndex;
                    int PrimitiveCount = m_Mesh.Meshes[j].MeshParts[i].PrimitiveCount;
                    int BaseVertex     = m_Mesh.Meshes[j].MeshParts[i].BaseVertex;

                    if (PrimitiveCount != 0)
                    {
                        //Set texture of the sub mesh
                        m_pEffect.SetTexture(0, ((BasicEffect)m_Mesh.Meshes[j].Effects[i]).Texture);

                        //Run thorugh all passes and make one draw call per pass
                        for (int p = 0; p < nPasses; p++)
                        {
                            m_pEffect.BeginPass(p);

                            //Render the sub mesh
                            m_pRenderEngine.GetGraphicsDevice().DrawIndexedPrimitives(PrimitiveType.TriangleList,
                                BaseVertex, 0, nVertices, StartIndex, PrimitiveCount);

                            m_pEffect.EndPass();
                        }
                    }
                }
            }

            //Reset effect
            m_pEffect.EndEffect();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////

        public void ComputeTangentSpace()
        {
            for (int m = 0; m < m_Mesh.Meshes.Count; m++)
            {
                ComputeTangentSpace(m);
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////

        public void ComputeTangentSpace(int m)
        {
            bool Index16Bit = m_Mesh.Meshes[m].IndexBuffer.IndexElementSize == IndexElementSize.SixteenBits;

            //Compute vertex and index count of the mesh
            int nVertices = m_Mesh.Meshes[m].VertexBuffer.SizeInBytes / MARSHAL.SizeOf(typeof(MESH_VERTEX));
         
            int nIndices = 0;

            if (Index16Bit)
            {
                nIndices = m_Mesh.Meshes[m].IndexBuffer.SizeInBytes / 2;
            }
            else
            {
                nIndices = m_Mesh.Meshes[m].IndexBuffer.SizeInBytes / 4;
            }

            //Get vertices and indices
            MESH_VERTEX[] pMeshDataVB = new MESH_VERTEX[nVertices];
            UInt16[]    pMeshData16IB = new UInt16[nIndices];
            UInt32[]    pMeshData32IB = new UInt32[nIndices];

           
            if (Index16Bit)
            {
                m_Mesh.Meshes[m].IndexBuffer.GetData<UInt16>(pMeshData16IB);
            }
            else
            {
                m_Mesh.Meshes[m].IndexBuffer.GetData<UInt32>(pMeshData32IB);
            }
            
            m_Mesh.Meshes[m].VertexBuffer.GetData<MESH_VERTEX>(pMeshDataVB);            

            //Clear Tangentspace
            for(int i=0; i<nVertices; i++)
            {
                pMeshDataVB[i].Tangent   = new Vector3(0.0f);
                pMeshDataVB[i].BiTangent = new Vector3(0.0f);
                //pMeshDataVB[i].Normal  = new Vector3(0.0f);
            }
	
            //Loop through all triangles, accumulating du and dv offsets
            //to build basis vectors
            for(int i=0; i<nIndices; i+=3)
            {
                UInt32 i0 = (Index16Bit) ? pMeshData16IB[i + 0] : pMeshData32IB[i + 0];
                UInt32 i1 = (Index16Bit) ? pMeshData16IB[i + 1] : pMeshData32IB[i + 1];
                UInt32 i2 = (Index16Bit) ? pMeshData16IB[i + 2] : pMeshData32IB[i + 2];

                MESH_VERTEX v0 = pMeshDataVB[i0];
                MESH_VERTEX v1 = pMeshDataVB[i1];
                MESH_VERTEX v2 = pMeshDataVB[i2];

                Vector3 du = new Vector3(0.0f);
                Vector3 dv = new Vector3(0.0f);
                Vector3 cp;

                Vector3 edge01 = new Vector3(v1.Position.X - v0.Position.X, v1.TexCoord.X - v0.TexCoord.X, v1.TexCoord.Y - v0.TexCoord.Y);
                Vector3 edge02 = new Vector3(v2.Position.X - v0.Position.X, v2.TexCoord.X - v0.TexCoord.X, v2.TexCoord.Y - v0.TexCoord.Y);
                cp = Vector3.Cross(edge01, edge02);
			
                if (Math.Abs(cp.X) > 1e-8)
                {
                    du.X = -cp.Y / cp.X;        
                    dv.X = -cp.Z / cp.X;
                }

                edge01 = new Vector3(v1.Position.Y - v0.Position.Y, v1.TexCoord.X - v0.TexCoord.X, v1.TexCoord.Y - v0.TexCoord.Y);
                edge02 = new Vector3(v2.Position.Y - v0.Position.Y, v2.TexCoord.X - v0.TexCoord.X, v2.TexCoord.Y - v0.TexCoord.Y);
                cp = Vector3.Cross(edge01, edge02);

                if (Math.Abs(cp.X) > 1e-8)
                {
                    du.Y = -cp.Y / cp.X;
                    dv.Y = -cp.Z / cp.X;
                }

                edge01 = new Vector3(v1.Position.Z - v0.Position.Z, v1.TexCoord.X - v0.TexCoord.X, v1.TexCoord.Y - v0.TexCoord.Y);
                edge02 = new Vector3(v2.Position.Z - v0.Position.Z, v2.TexCoord.X - v0.TexCoord.X, v2.TexCoord.Y - v0.TexCoord.Y);
                cp = Vector3.Cross(edge01, edge02);

                if (Math.Abs(cp.X) > 1e-8)
                {
                    du.Z = -cp.Y / cp.X;
                    dv.Z = -cp.Z / cp.X;
                }

                pMeshDataVB[i0].Tangent += du;
                pMeshDataVB[i1].Tangent += du;
                pMeshDataVB[i2].Tangent += du;

                pMeshDataVB[i0].BiTangent += dv;
                pMeshDataVB[i1].BiTangent += dv;
                pMeshDataVB[i2].BiTangent += dv;
            }

            for(int i=0; i<nVertices; i++)
            {
                pMeshDataVB[i].Tangent = Vector3.Normalize(pMeshDataVB[i].Tangent);
                pMeshDataVB[i].BiTangent = Vector3.Normalize(pMeshDataVB[i].BiTangent);
                //pMeshDataVB[i].Normal = Vector3.Cross(pMeshDataVB[i].Tangent, pMeshDataVB[i].BiTangent);

                //Get the vertex normal (make sure it's normalized)
                //Make sure the basis vector and normal point in the same direction
                //if (Vector3.Dot(pMeshDataVB[i].Normal, pMeshDataVB[i].n) < 0.0f)
                //{
                //    pMeshDataVB[i].Normal = -pMeshDataVB[i].Normal;
                //}
            }

            m_Mesh.Meshes[m].VertexBuffer.SetData<MESH_VERTEX>(pMeshDataVB);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////
    }
}