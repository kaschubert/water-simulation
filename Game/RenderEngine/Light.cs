#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
#endregion


class CLight
{
	public Vector3 m_Position;
    public Vector3 m_Ambient;
    public Vector3 m_Diffuse;
    public Vector3 m_Attenuation;

    public CLight(Vector3 Position, Vector3 Ambient, Vector3 Diffuse, Vector3 Attenuation)
	{
        m_Position    = Position;
        m_Ambient     = Ambient;
        m_Diffuse     = Diffuse;
        m_Attenuation = Attenuation;
	}
}
