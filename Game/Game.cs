
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
#endregion

namespace Game
{
	/// <summary>
	/// This is the main type for your game
	/// </summary>
	public class Game : Microsoft.Xna.Framework.Game
	{
		GraphicsDeviceManager m_Graphics;
		public static ContentManager m_ContentManager;
		
		CRenderEngine m_RenderEngine;
        int           m_MouseX;
        int           m_MouseY;
        bool          m_Pause;


		public Game()
		{
            m_Graphics = new GraphicsDeviceManager(this);
            m_ContentManager = new ContentManager(Services);

            //Use this to check display modes ...
            //GraphicsAdapter.DefaultAdapter
            
            m_Graphics.PreferMultiSampling       = false;//true;
            m_Graphics.PreferredBackBufferWidth  = 1024;
            m_Graphics.PreferredBackBufferHeight = 768;
            
            m_Graphics.ApplyChanges();

            m_Graphics.PreparingDeviceSettings += new EventHandler<PreparingDeviceSettingsEventArgs>(PreparingDeviceSettingsCallBack);

            //Allow user to resize the window
            Window.AllowUserResizing = false;
            
            m_MouseX = 0;
            m_MouseY = 0;
            m_Pause  = false;
		}

        void PreparingDeviceSettingsCallBack(object sender, PreparingDeviceSettingsEventArgs e)
        {
            // Xbox 360 and most PCs support FourSamples/0 (4x) and TwoSamples/0 (2x)
            // antialiasing.
            #if XBOX
                e.GraphicsDeviceInformation.PresentationParameters.MultiSampleQuality = 0;
                e.GraphicsDeviceInformation.PresentationParameters.MultiSampleType =
                    MultiSampleType.FourSamples;
                return;
            #endif

            //Backbuffer should not be cleared if an other rendertarget will be set (Only possible on PC)
            e.GraphicsDeviceInformation.PresentationParameters.RenderTargetUsage = RenderTargetUsage.PreserveContents;

            int quality = 0;

            GraphicsAdapter adapter = e.GraphicsDeviceInformation.Adapter;
            SurfaceFormat format = adapter.CurrentDisplayMode.Format;

            //Check for AA support
            //if (adapter.CheckDeviceMultiSampleType(DeviceType.Hardware, format, false, MultiSampleType.TwoSamples, out quality))
            //{
                //Even if a greater quality is returned, we only want quality 0
            //    e.GraphicsDeviceInformation.PresentationParameters.MultiSampleQuality = 0;
            //    e.GraphicsDeviceInformation.PresentationParameters.MultiSampleType = MultiSampleType.FourSamples;
            //}
            return;

        }

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			// TODO: Add your initialization logic here

			base.Initialize();
		}


		/// <summary>
		/// Load your graphics content.  If loadAllContent is true, you should
		/// load content from both ResourceManagementMode pools.  Otherwise, just
		/// load ResourceManagementMode.Manual content.
		/// </summary>
		/// <param name="loadAllContent">Which type of content to load.</param>
		protected override void LoadGraphicsContent(bool loadAllContent)
		{
            if (m_RenderEngine == null)
            {
                m_RenderEngine = new CRenderEngine(m_Graphics);
            }

            if (loadAllContent)
            {
                // TODO: Load any ResourceManagementMode.Automatic content
                m_RenderEngine.LoadContent();
            }
            else
            {
                m_RenderEngine.ResetDevice();
            }

			// TODO: Load any ResourceManagementMode.Manual content
		}


		/// <summary>
		/// Unload your graphics content.  If unloadAllContent is true, you should
		/// unload content from both ResourceManagementMode pools.  Otherwise, just
		/// unload ResourceManagementMode.Manual content.  Manual content will get
		/// Disposed by the GraphicsDevice during a Reset.
		/// </summary>
		/// <param name="unloadAllContent">Which type of content to unload.</param>
		protected override void UnloadGraphicsContent(bool unloadAllContent)
		{
			if (unloadAllContent == true)
			{
                m_ContentManager.Unload();
			}

            if (m_RenderEngine != null)
            {
                m_RenderEngine.LostDevice();
            }
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			// Allows the default game to exit on Xbox 360 and Windows
            base.Update(gameTime);

            if (m_RenderEngine == null)
            {
                return;
            }

            float fElapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;// gameTime.ElapsedRealTime.TotalSeconds;

			//Check if ESC is pressed
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }

			if (Keyboard.GetState().IsKeyDown(Keys.F5) && (m_RenderEngine != null))
			{
                m_RenderEngine.GetShaderManager().Reload();
			}

            float dX = (float)(Mouse.GetState().X - m_MouseX);
            float dY = (float)(Mouse.GetState().Y - m_MouseY);

            if (Mouse.GetState().RightButton == ButtonState.Pressed)
            {
                m_RenderEngine.GetCamera().RotateAroundPosition(-fElapsedTime * dX, -fElapsedTime * dY);
            }
           
            m_MouseX = Mouse.GetState().X;
            m_MouseY = Mouse.GetState().Y;
            
            float Move = 64.0f;
            float Rotate = 1.5f;

			if(Keyboard.GetState().IsKeyDown(Keys.D))
                m_RenderEngine.GetCamera().AddToCamera( Move * fElapsedTime, 0.0f, 0.0f);

			if (Keyboard.GetState().IsKeyDown(Keys.A))
                m_RenderEngine.GetCamera().AddToCamera(-Move * fElapsedTime, 0.0f, 0.0f);

			if (Keyboard.GetState().IsKeyDown(Keys.W))
                m_RenderEngine.GetCamera().AddToCamera(0.0f, 0.0f,  Move * fElapsedTime);

			if (Keyboard.GetState().IsKeyDown(Keys.S))
                m_RenderEngine.GetCamera().AddToCamera(0.0f, 0.0f, -Move * fElapsedTime);

			if (Keyboard.GetState().IsKeyDown(Keys.Left))
                m_RenderEngine.GetCamera().RotateAroundPosition( Rotate * fElapsedTime, 0.0f);

			if (Keyboard.GetState().IsKeyDown(Keys.Right))
                m_RenderEngine.GetCamera().RotateAroundPosition(-Rotate * fElapsedTime, 0.0f);

			if (Keyboard.GetState().IsKeyDown(Keys.Up))
                m_RenderEngine.GetCamera().RotateAroundPosition(0.0f,  Rotate * fElapsedTime);

			if (Keyboard.GetState().IsKeyDown(Keys.Down))
                m_RenderEngine.GetCamera().RotateAroundPosition(0.0f, -Rotate * fElapsedTime);

            if (Keyboard.GetState().IsKeyDown(Keys.P))
                m_Pause = !m_Pause;

            if (m_Pause)
            {
                fElapsedTime = 0.0f;
            }

            m_RenderEngine.Update(fElapsedTime);
		}


		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			base.Draw(gameTime);

            if (m_RenderEngine != null)
            {
                m_RenderEngine.Render();
            }
		}
	}
}