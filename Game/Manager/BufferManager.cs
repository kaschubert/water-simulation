#region Using Statements
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
#endregion

////////////////////////////////////////////////////////////////////////////////////////////

class CVertexBuffer
{
    CRenderEngine m_pRenderEngine;
    VertexBuffer  m_pBuffer;
    int           m_BufferSize;
    int           m_VertexSize;

    public CVertexBuffer()
    {
        Clear();
    }

    ~CVertexBuffer()
    {
        Clear();
    }

    public void Create(CRenderEngine pRenderEngine, int BufferSize, int VertexSize)
    {
        Clear();

        m_pRenderEngine = pRenderEngine;
        m_BufferSize    = BufferSize;
        m_VertexSize    = VertexSize;

        m_pBuffer = new VertexBuffer(pRenderEngine.GetGraphicsDevice(), BufferSize, BufferUsage.WriteOnly);
    }

    public void Clear()
    {
        m_pRenderEngine = null;
        m_pBuffer       = null;
        m_BufferSize    = 0;
        m_VertexSize    = 0;
    }

    public void SetData<T>(T[] pData) where T : struct
    { 
        m_pBuffer.SetData<T>(pData);
    }

    public void SetBuffer(int Stage)
    {
        SetBuffer(Stage, 0);
    }

    public void SetBuffer(int Stage, int Offset)
    {
        m_pRenderEngine.GetGraphicsDevice().Vertices[Stage].SetSource(
            m_pBuffer, Offset, m_VertexSize);
    }
};

////////////////////////////////////////////////////////////////////////////////////////////

class CIndexBuffer
{
    CRenderEngine m_pRenderEngine;
    IndexBuffer   m_pBuffer;
    int           m_BufferSize;
    int           m_IndexSize;

    public CIndexBuffer()
    {
        Clear();
    }

    ~CIndexBuffer()
    {
        Clear();
    }

    public void Create(CRenderEngine pRenderEngine, int BufferSize, int IndexSize)
    {
        Clear();

        m_pRenderEngine = pRenderEngine;
        m_BufferSize    = BufferSize;
        m_IndexSize     = IndexSize;
        
        if (IndexSize == 2)
        {
            m_pBuffer = new IndexBuffer(pRenderEngine.GetGraphicsDevice(), BufferSize, BufferUsage.WriteOnly, IndexElementSize.SixteenBits);
        }
        else if (IndexSize == 4)
        {
            m_pBuffer = new IndexBuffer(pRenderEngine.GetGraphicsDevice(), BufferSize, BufferUsage.WriteOnly, IndexElementSize.ThirtyTwoBits);
        }
        else
        {
            //FAILED: One index must have a size of two or four bytes
        }
    }

    public void Clear()
    {
        m_pRenderEngine = null;
        m_pBuffer       = null;
        m_BufferSize    = 0;
        m_IndexSize     = 0;
    }

    public void SetData<T>(T[] pData) where T : struct
    {
        m_pBuffer.SetData<T>(pData);
    }

    public void SetBuffer()
    {
        m_pRenderEngine.GetGraphicsDevice().Indices = m_pBuffer;
    }
};

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

class CBufferManager
{
    CRenderEngine       m_pRenderEngine;
    List<CVertexBuffer> m_VertexBuffers;
    List<CIndexBuffer>  m_IndexBuffers;

    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    public CBufferManager(CRenderEngine pRenderEngine)
    {
        m_pRenderEngine = pRenderEngine;
        m_VertexBuffers = new List<CVertexBuffer>();
        m_IndexBuffers  = new List<CIndexBuffer>();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    ~CBufferManager()
    {
        for (int i = 0; i < m_VertexBuffers.Count; i++)
        {
            m_VertexBuffers[i].Clear();
        }

        for (int i = 0; i < m_IndexBuffers.Count; i++)
        {
            m_IndexBuffers[i].Clear();
        }
        
        m_VertexBuffers.Clear();
        m_IndexBuffers.Clear();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public CVertexBuffer GetVertexBuffer(int VertexSize, int VertexCount)
    {
        CVertexBuffer pVertexBuffer = new CVertexBuffer();
        
        if (pVertexBuffer != null)
        {
            pVertexBuffer.Create(m_pRenderEngine, VertexCount * VertexSize, VertexSize);

            m_VertexBuffers.Add(pVertexBuffer);

            return pVertexBuffer;
        }
       
        return null;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public CIndexBuffer GetIndexBuffer(int IndexSize, int IndexCount)
    {
        CIndexBuffer pIndexBuffer = new CIndexBuffer();

        if (pIndexBuffer != null)
        {
            pIndexBuffer.Create(m_pRenderEngine, IndexCount * IndexSize, IndexSize);

            m_IndexBuffers.Add(pIndexBuffer);

            return pIndexBuffer;
        }

        return null;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void RemoveVertexBuffer(CVertexBuffer pVertexBuffer)
    {
        for (int i = 0; i < m_VertexBuffers.Count; i++)
        {
            if (m_VertexBuffers[i] == pVertexBuffer)
            {
                m_VertexBuffers.RemoveAt(i);

                pVertexBuffer.Clear();
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void RemoveIndexBuffer(CIndexBuffer pIndexBuffer)
    {
        for (int i = 0; i < m_IndexBuffers.Count; i++)
        {
            if (m_IndexBuffers[i] == pIndexBuffer)
            {
                m_IndexBuffers.RemoveAt(i);

                pIndexBuffer.Clear();
            }
        }
    }
};

////////////////////////////////////////////////////////////////////////////////////////////