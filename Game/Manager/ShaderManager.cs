#region Using Statements
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
#endregion

////////////////////////////////////////////////////////////////////////////////////////////

class CEffect
{
    public String              sPath;
    public Effect              pEffect;
    public GraphicsDevice      pGraphicsDevice;
    public VertexDeclaration[] pVertexDeclarations;
    
    int CurPass = 0;
    int CurTechnique = 0;

    //Paramter

    //Matrices
    public EffectParameter pParamW;
    public EffectParameter pParamV;
    public EffectParameter pParamP;
    public EffectParameter pParamVP;
    public EffectParameter pParamT;
    public EffectParameter pParamR0;

    //Camera
    public EffectParameter pParamCamPos;
    public EffectParameter pParamCamDir;

    //Light
    public EffectParameter pParamLightPos;
    public EffectParameter pParamLightDiffuse;
    public EffectParameter pParamLightAbmient;
	public EffectParameter pParamLightAttenuation;
	
	//Shadow
	public EffectParameter pParamShadowVP;
    public EffectParameter pParamShadowVPP;

    //Global Parameter
    public EffectParameter pParamTime;

    EffectParameter[] pParamTextures = new EffectParameter[16];

    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    public CEffect(String _strPath, GraphicsDevice _pGraphicsDevice)
    {
        pGraphicsDevice     = _pGraphicsDevice;
        pVertexDeclarations = null;
        sPath = new String(_strPath.ToCharArray());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void InitParams()
    {
        if (pEffect == null)
        {
            return;
        }

        pParamW  = pEffect.Parameters["matW"];
        pParamV  = pEffect.Parameters["matV"];
        pParamP  = pEffect.Parameters["matP"];
        pParamVP = pEffect.Parameters["matVP"];
        pParamT  = pEffect.Parameters["matT"];
        pParamR0 = pEffect.Parameters["R0"];

        pParamCamPos = pEffect.Parameters["cCamPos"];
        pParamCamDir = pEffect.Parameters["cCamDir"];

        pParamTime = pEffect.Parameters["cTime"];

        for (int i = 0; i < 16; i++)
        {
            pParamTextures[i] = pEffect.Parameters["Texture" + i.ToString()];
        }

        pParamLightPos     = pEffect.Parameters["cLightPos"];
        pParamLightDiffuse = pEffect.Parameters["cLightDiffuse"];
        pParamLightAbmient = pEffect.Parameters["cLightAmbient"];
		pParamLightAttenuation = pEffect.Parameters["cLightAttenuation"];

		pParamShadowVP  = pEffect.Parameters["matShadowVP"];
		pParamShadowVPP = pEffect.Parameters["matShadowVPP"];
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void SetWorldMatrix(Matrix matW)
    {
        pParamW.SetValue(matW);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

	public void SetTexture(int Stage, Texture pTexture)
    {
        if ((Stage >= 0) && (Stage < 16))
        {
            pParamTextures[Stage].SetValue(pTexture);
        }
    }

	////////////////////////////////////////////////////////////////////////////////////////////

    public void SetTexture(int Stage, CTexture pTexture)
    {
        if (pTexture == null)
        {
            return;
        }

        if ((Stage >= 0) && (Stage < 16))
        {
            pParamTextures[Stage].SetValue(pTexture.m_pTexture);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void SetTexture(int Stage, CRendertarget pRenderTarget)
    {
        if (pRenderTarget == null || pEffect == null)
        {
            return;
        }

        if ((Stage >= 0) && (Stage < 16))
        {
            pParamTextures[Stage].SetValue(pRenderTarget.pTexture);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    
    public int BeginEffect()
    {
        return BeginEffect(0);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public int BeginEffect(int Technique)
    {
        if (null != pEffect && Technique < pEffect.Techniques.Count)
        {
            CurTechnique = Technique;

            //Set current technique
            pEffect.CurrentTechnique = pEffect.Techniques[Technique];

            //Begin effect (save Samplerstates, Renderstates, ...)
            pEffect.Begin(SaveStateMode.SaveState);

            //Set suitable vertex declaration
            pGraphicsDevice.VertexDeclaration = pVertexDeclarations[Technique];

            return pEffect.Techniques[Technique].Passes.Count;
        }

        return 0;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void EndEffect()
    {
        if (null != pEffect && CurTechnique < pEffect.Techniques.Count)
        {
            //End complete effect (reset saved sampler states, render states, ...)
            pEffect.End();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void BeginPass(int Pass)
    {
        if (null != pEffect)
        {
            CurPass = Pass;

            //Begin pass (set shader and new sampler states, render states, ...)
            pEffect.Techniques[CurTechnique].Passes[CurPass].Begin();
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////

    public void EndPass()
    {
        if (null != pEffect)
        {
            //End pass (reset shader and sampler states, render states, ...)
            pEffect.Techniques[CurTechnique].Passes[CurPass].End();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void CommitChanges()
    {
        if (null != pEffect)
        {
            pEffect.CommitChanges();
        }        
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    
    public void SetParameter(String Name, float Parameter)
    {
        pEffect.Parameters[Name].SetValue(Parameter);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void SetParameter(String Name, Vector2 Parameter)
    {
        pEffect.Parameters[Name].SetValue(Parameter);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void SetParameter(String Name, Vector3 Parameter)
    {
        pEffect.Parameters[Name].SetValue(Parameter);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void SetParameter(String Name, Vector4 Parameter)
    {
        pEffect.Parameters[Name].SetValue(Parameter);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void SetParameter(String Name, Matrix Parameter)
    {
        pEffect.Parameters[Name].SetValue(Parameter);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
};


////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

class CShaderManager
{
    CRenderEngine  m_pRenderEngine;

    ArrayList      m_Effects;
    ArrayList      m_VertexDeclarations;


    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    public CShaderManager(CRenderEngine pRenderEngine)
    {
        m_pRenderEngine      = pRenderEngine;
        m_Effects            = new ArrayList();
        m_VertexDeclarations = new ArrayList();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    ~CShaderManager()
    {
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////

    public CEffect GetEffect(String strPath)
    {
        return GetEffect(strPath, "");
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public CEffect GetEffect(String sPath, string sVertexDeclarationDescription)
    {
        //Search for existing effects
        sPath = sPath.ToLower();

        for (int i=0; i < m_Effects.Count; i++)
        {
            if (((CEffect)m_Effects[i]).sPath == sPath)
            {
                return (CEffect)m_Effects[i];
            }
        }

        //Compile new effect file
        CompiledEffect pCompiledEffect = Effect.CompileEffectFromFile(sPath, null, null, CompilerOptions.None, TargetPlatform.Windows);

        CEffect pEffect = new CEffect(sPath, m_pRenderEngine.GetGraphicsDevice());

        try
        {
            pEffect.pEffect = new Effect(m_pRenderEngine.GetGraphicsDevice(), pCompiledEffect.GetEffectCode(), CompilerOptions.None, null);
            pEffect.InitParams();
			
			pEffect.pVertexDeclarations = new VertexDeclaration[pEffect.pEffect.Techniques.Count];

			//Find vertex declaration descriptions inside the fx file
			for (int i = 0; i < pEffect.pEffect.Techniques.Count; i++)
			{
				//Maybe global declaration exist
				EffectParameter  GlobalVertexDecl    = pEffect.pEffect.Parameters["VertexDeclaration"];
				EffectAnnotation TechniqueVertexDecl = pEffect.pEffect.Techniques[i].Annotations["VertexDeclaration"];

				int VertexDeclarationIndex = -1;

				if (sVertexDeclarationDescription != "")
				{
					VertexDeclarationIndex = GetVertexDeclarationIndex(sVertexDeclarationDescription);
				}
				else if (TechniqueVertexDecl != null)
				{
					VertexDeclarationIndex = GetVertexDeclarationIndex(TechniqueVertexDecl.GetValueString());
				}
				else if (GlobalVertexDecl != null)
				{
					VertexDeclarationIndex = GetVertexDeclarationIndex(GlobalVertexDecl.GetValueString());
				}

				if (VertexDeclarationIndex >= 0 && VertexDeclarationIndex < m_VertexDeclarations.Count)
				{
					pEffect.pVertexDeclarations[i] = (VertexDeclaration)m_VertexDeclarations[VertexDeclarationIndex];
				}
			}
        
            m_Effects.Add(pEffect);
        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine(ex.Message);
            System.Diagnostics.Debug.WriteLine(pCompiledEffect.ErrorsAndWarnings);
        }

        return pEffect;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void UpdateParameters()
    {
        if (m_pRenderEngine == null)
        {
            return;
        }

        for (int i = 0; i < m_Effects.Count; i++)
        {
            CEffect pEffect = (CEffect)m_Effects[i];

            //Matrices
            if (pEffect.pParamV != null)
            {
                pEffect.pParamV.SetValue(m_pRenderEngine.GetCamera().m_matView);
            }

            if (pEffect.pParamP != null)
            {
                pEffect.pParamP.SetValue(m_pRenderEngine.GetCamera().m_matProj);
            }

            if (pEffect.pParamVP != null)
            {
                pEffect.pParamVP.SetValue(m_pRenderEngine.GetCamera().m_matViewProj);
            }

            if (pEffect.pParamT != null)
            {
                Matrix matT = new Matrix(
                        0.5f,  0.0f,  0.0f,  0.0f,
						0.0f, -0.5f,  0.0f,  0.0f,
						0.0f,  0.0f,  0.5f,  0.0f,
						0.5f,  0.5f,  0.5f,  1.0f
                    );

                pEffect.pParamT.SetValue(matT);
            }

            if (pEffect.pParamR0 != null)
            {
                float r0 = (float) (Math.Pow(0.5/*1.000293 - 1.333333*/, 2) / Math.Pow(2.5/*1.000293 + 1.333333*/, 2));

                pEffect.pParamR0.SetValue(r0);
            }

            //Camera
            if (pEffect.pParamCamPos != null)
            {
                pEffect.pParamCamPos.SetValue(m_pRenderEngine.GetCamera().m_Position);
            }

            if (pEffect.pParamCamDir != null)
            {
                pEffect.pParamCamDir.SetValue(m_pRenderEngine.GetCamera().m_Direction);
            }

            //Settings
            if (pEffect.pParamTime != null)
            {
                float LoopedTime = (float)(m_pRenderEngine.GetTime() - (int)m_pRenderEngine.GetTime());

                Vector4 Time = new Vector4(m_pRenderEngine.GetTime(), LoopedTime, m_pRenderEngine.GetElapsedTime(), 0.0f);

                pEffect.pParamTime.SetValue(Time);
            }

            //Lights
            CLight pLight = m_pRenderEngine.GetLight();

            if (pEffect.pParamLightPos != null)
            {
                pEffect.pParamLightPos.SetValue(pLight.m_Position);
            }

            if (pEffect.pParamLightAbmient != null)
            {
                pEffect.pParamLightAbmient.SetValue(pLight.m_Ambient);  
            }

            if (pEffect.pParamLightDiffuse != null)
            {
                pEffect.pParamLightDiffuse.SetValue(pLight.m_Diffuse);
            }

            if (pEffect.pParamLightAttenuation != null)
            {
                pEffect.pParamLightAttenuation.SetValue(pLight.m_Attenuation);
            }            
			
			//Shadow
            if (pEffect.pParamShadowVP != null)
            {
                pEffect.pParamShadowVP.SetValue(m_pRenderEngine.GetShadowMap().GetMatrixVP());
            }

            if (pEffect.pParamShadowVPP != null)
            {
                pEffect.pParamShadowVPP.SetValue(m_pRenderEngine.GetShadowMap().GetMatrixVPP());
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    int GetVertexDeclarationIndex(String sVertexDeclaration)
    {
        //Parse description string to create the vertexdeclaration

        int Symbol = 0; //0 = Register usage
        //1 = Register count

        List<VertexElement> VertexElements = new List<VertexElement>();

        //VertexElement parameter
        int Stream = 0;
        int Offset = 0;
        int UsageIndex = 0;
        int[] UsageIndices = { 0, 0, 0, 0, 0 };
        VertexElementUsage Usage = VertexElementUsage.Position;
        VertexElementFormat Format = VertexElementFormat.Single;

        for (int i = 0; i < sVertexDeclaration.Length; i++)
        {
            if (Symbol == 0)
            {
                switch (sVertexDeclaration[i])
                {
                    case 'P': Usage = VertexElementUsage.Position;
                        UsageIndex = UsageIndices[0];
                        UsageIndices[0] += 1;
                        break;
                    case 'T': Usage = VertexElementUsage.TextureCoordinate;
                        UsageIndex = UsageIndices[1];
                        UsageIndices[1] += 1;
                        break;
                    case 'N': Usage = VertexElementUsage.Normal;
                        UsageIndex = UsageIndices[2];
                        UsageIndices[2] += 1;
                        break;
                    case 'B': Usage = VertexElementUsage.Binormal;
                        UsageIndex = UsageIndices[3];
                        UsageIndices[3] += 1;
                        break;
                    case 'Z': Usage = VertexElementUsage.Tangent;
                        UsageIndex = UsageIndices[4];
                        UsageIndices[4] += 1;
                        break;
                    //case "C": Usage = VertexElementUsage.Color; break;  Other offset!

                    case ',':
                        Stream += 1;    //New stream
                        Offset = 0;    //Reset offset
                        break;

                    default: //Undefined symbol
                        return -1;
                }

                if (sVertexDeclaration[i] != ',')
                {
                    Symbol = 1; //Step to count symbol
                }
            }
            else
            {
                int PreOffset = Offset;

                switch (sVertexDeclaration[i])
                {
                    case '1': Format = VertexElementFormat.Single;
                        Offset = Offset + 4;
                        break;
                    case '2': Format = VertexElementFormat.Vector2;
                        Offset = Offset + 8;
                        break;
                    case '3': Format = VertexElementFormat.Vector3;
                        Offset = Offset + 12;
                        break;
                    case '4': Format = VertexElementFormat.Vector4;
                        Offset = Offset + 16;
                        break;

                    default: //Invalid register count
                        return -1;
                }

                VertexElement VE = new VertexElement((short)Stream, (short)PreOffset, Format,
                                            VertexElementMethod.Default, Usage, (byte)UsageIndex);

                VertexElements.Add(VE);

                Symbol = 0; //Step to usage symbol
            }
        }

        if (VertexElements.Count > 0)
        {
            VertexElement[] pVertexElements = VertexElements.ToArray();

            m_VertexDeclarations.Add(new VertexDeclaration(m_pRenderEngine.GetGraphicsDevice(), pVertexElements));

            //Return ID of the vertex declaration
            return m_VertexDeclarations.Count - 1;
        }

        return -1;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

	public void Reload()
	{
        for (int i = 0; i < m_Effects.Count; i++)
        {
            CEffect pEffect = (CEffect)m_Effects[i];
            CompiledEffect pCompiledEffect;

            try
            {
                pCompiledEffect = Effect.CompileEffectFromFile(pEffect.sPath, null, null, CompilerOptions.None, TargetPlatform.Windows);
                pEffect.pEffect = new Effect(m_pRenderEngine.GetGraphicsDevice(), pCompiledEffect.GetEffectCode(), CompilerOptions.None, null);
                pEffect.InitParams();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(pCompiledEffect.ErrorsAndWarnings);
            }
        }
	}

	////////////////////////////////////////////////////////////////////////////////////////////
};