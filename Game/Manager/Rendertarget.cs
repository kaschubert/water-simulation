#region Using Statements
using System;
using System.IO;
using System.Collections;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
#endregion

enum USED_ZBUFFER
{
	UZ_LASTZBUFFER, //Use current z buffer
	UZ_ZBUFFER,		//Use own z buffer
	UZ_NOZBUFFER,	//Don't use any z buffer (null)

	UZ_END
};

class CRendertarget
{
	public GraphicsDevice     m_pGraphicsDevice;
		
	public USED_ZBUFFER	      eUsedZBuffer;

	public Texture2D          pTexture;
	public RenderTarget2D     pSurface;
	public DepthStencilBuffer pZBuffer;
	public Viewport           pViewport;

    //Restore Data
	public Viewport		      pSaveViewport;
	public RenderTarget       pSaveBackBuffer;
	public DepthStencilBuffer pSaveZBuffer;
	
	//Settings of the rendertarget
    public uint               Width;
    public uint               Height;
	public SurfaceFormat	  Format;
	
	public int				  Stage;

	public CRendertarget()
	{
		Stage			= 0;
		Height          = 0;
		Width           = 0;
		pTexture		= null;
		pSurface    	= null;
		pZBuffer		= null;
		pSaveBackBuffer = null;
		pSaveZBuffer	= null;
		eUsedZBuffer    = USED_ZBUFFER.UZ_NOZBUFFER;
	}

	public bool OwnZBuffer()
	{
        if (pZBuffer != null)
        {
            return true;
        }

		return false;
	}

    public void Create(GraphicsDevice  pGraphicsDevice)
    {
        Create(pGraphicsDevice, true);
    }
	
	public bool Create(GraphicsDevice  pGraphicsDevice, bool bDepthbuffer/*= true*/)
	{
		m_pGraphicsDevice = pGraphicsDevice;

        try
        {
            pSurface = new RenderTarget2D(pGraphicsDevice, (int)Width, (int)Height, 1,
				                          Format, MultiSampleType.None, 0, RenderTargetUsage.PreserveContents);
        }
        catch (Exception e)
        {
            System.Diagnostics.Debug.WriteLine(e.Message);
        }
		
        pViewport = new Viewport();

        pViewport.X        = 0;
        pViewport.Y        = 0;
        pViewport.MinDepth = 0.0f;
        pViewport.MaxDepth = 1.0f;
        pViewport.Width    = (int)Width;
        pViewport.Height   = (int)Height;
        
		if (bDepthbuffer)
		{
			eUsedZBuffer = USED_ZBUFFER.UZ_ZBUFFER;

			pZBuffer = new DepthStencilBuffer(m_pGraphicsDevice, (int)Width, (int)Height, 
				                        DepthFormat.Depth24Stencil8, MultiSampleType.None, 0);
		}
        
        return true;
	}

	public void SetTexture(int Sampler)
	{
        m_pGraphicsDevice.Textures[Sampler] = pTexture;
	}

	public bool BeginScene()
	{
		return BeginScene(0);
	}

	public bool BeginScene(int _Stage)
	{
		if (m_pGraphicsDevice == null || pSurface == null)
		{
            return false;
        }

		Stage = _Stage;

        //Store Buffers and Viewport
        pSaveBackBuffer = m_pGraphicsDevice.GetRenderTarget(Stage);

		if (Stage == 0)
		{
			pSaveZBuffer  = m_pGraphicsDevice.DepthStencilBuffer;
			pSaveViewport = m_pGraphicsDevice.Viewport;
	        
			//Set new Buffers and Viewport
			m_pGraphicsDevice.Viewport = pViewport;
		}

        m_pGraphicsDevice.SetRenderTarget(Stage, pSurface);

		if (Stage == 0)
		{
			if (pZBuffer != null) //UZ_ZBUFFER
			{
				m_pGraphicsDevice.DepthStencilBuffer = pZBuffer;
			}
			else if (eUsedZBuffer == USED_ZBUFFER.UZ_LASTZBUFFER) //UZ_LASTZBUFFER
			{
				m_pGraphicsDevice.DepthStencilBuffer = pSaveZBuffer;
			}
			else //UZ_NOZBUFFER
			{
				m_pGraphicsDevice.DepthStencilBuffer = null;
			}
		}
		
		return true;
	}

	public bool EndScene()
	{
        if (m_pGraphicsDevice == null)
        {
            return false;
        }

        //Restore Buffers and Viewports
		if (Stage == 0)
		{
			m_pGraphicsDevice.Viewport			 = pSaveViewport;
			m_pGraphicsDevice.DepthStencilBuffer = pSaveZBuffer;
		}

        m_pGraphicsDevice.SetRenderTarget(Stage, (RenderTarget2D)pSaveBackBuffer);

        pTexture = pSurface.GetTexture();

		return true;
	}
};