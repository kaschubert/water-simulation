#region Using Statements
using System;
using System.IO;
using System.Collections;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
#endregion

using DWORD = System.UInt32;

////////////////////////////////////////////////////////////////////////////////////////////

enum TEXTURE_TYPE
{
    TEX2D = 0,
    TEX3D = 1,
    CUBE  = 2,

    COUNT
}
////////////////////////////////////////////////////////////////////////////////////////////

class CTexture
{
    CRenderEngine		  m_pRenderEngine;
    public TEXTURE_TYPE   m_Type;
    public String         m_Path;
    public Texture        m_pTexture;

    public CTexture(CRenderEngine pRenderEngine, String Path)
    {
        m_pRenderEngine = pRenderEngine;

        m_Path = new String(Path.ToCharArray());
    }

    public void SetTexture(int Sampler)
	{
        m_pRenderEngine.GetGraphicsDevice().Textures[Sampler] = m_pTexture;
	}

    public bool SetData<T>(T[] pData, int StartIndex, int ElementCount) where T : struct
    {
        if (m_pTexture != null && m_Type == TEXTURE_TYPE.TEX2D)
        {
            ((Texture2D)m_pTexture).SetData<T>(pData, StartIndex, ElementCount, SetDataOptions.None);

            return true;
        }

        return false;
    }

    public bool GetData<T>(T[] pData, int StartIndex, int ElementCount) where T : struct
    {
        if (m_pTexture != null && m_Type == TEXTURE_TYPE.TEX2D)
        {
            ((Texture2D)m_pTexture).GetData<T>(pData, StartIndex, ElementCount);

            return true;
        }

        return false;
    }
};

////////////////////////////////////////////////////////////////////////////////////////////

class CTextureManager
{
    //System.Collections
    ArrayList      m_pTextures;
    ArrayList      m_pRendertargets;

    CRenderEngine  m_pRenderEngine;

    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    public CTextureManager(CRenderEngine pRenderEngine)
    {
        m_pRenderEngine  = pRenderEngine;
        m_pTextures      = new ArrayList();
        m_pRendertargets = new ArrayList();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    ~CTextureManager()
    {
        //Release Textures
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////

    public CTexture GetTexture(string Path)
    {
        //Search if Textures exist
        for( int i=0; i < m_pTextures.Count; i++ )
        {
            if( ((CTexture)m_pTextures[i]).m_Path == Path )
            {
                return (CTexture)m_pTextures[i];
            }
        }
	
        CTexture pTexture = new CTexture(m_pRenderEngine, Path);

        pTexture.m_pTexture = Texture.FromFile(m_pRenderEngine.GetGraphicsDevice(), Path);

        switch (pTexture.m_pTexture.ResourceType)
        {
            case ResourceType.Texture2D:   pTexture.m_Type = TEXTURE_TYPE.TEX2D; break;
            case ResourceType.Texture3D:   pTexture.m_Type = TEXTURE_TYPE.TEX3D; break;
            case ResourceType.TextureCube: pTexture.m_Type = TEXTURE_TYPE.CUBE; break;
        }

        m_pTextures.Add(pTexture);
		
        return pTexture;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public CRendertarget GetRendertarget(SurfaceFormat Format, uint Width, uint Height)
    {
        return GetRendertarget(Format, Width, Height, false);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public CRendertarget GetRendertarget(SurfaceFormat Format, uint Width, uint Height, bool zBuffer/*= false*/)
    {
        if (m_pRenderEngine == null)
        {
            return null;
        }
	   
        CRendertarget pRenderTarget = new CRendertarget();

        if (Width == 0)
        {
            Width = (uint)m_pRenderEngine.GetGraphicsManager().PreferredBackBufferWidth;
        }

        if (Height == 0)
        {
            Height = (uint)m_pRenderEngine.GetGraphicsManager().PreferredBackBufferHeight;
        }

        pRenderTarget.Width  = Width;
        pRenderTarget.Height = Height;
        pRenderTarget.Format = Format;

        if (pRenderTarget.Create(m_pRenderEngine.GetGraphicsDevice(), zBuffer) == false)
	    {
		    return null;
	    }

        m_pRendertargets.Add(pRenderTarget);

        return pRenderTarget;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

};
