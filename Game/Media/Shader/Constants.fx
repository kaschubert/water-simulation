////////////////////////////////////////////////////////////////////
//Scene Constants///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

//Matrices
float4x4 matW;
float4x4 matN;
float4x4 matV;
float4x4 matP;
float4x4 matVP;
//Projection Space to Texture Space
float4x4 matT = 0;

//Inverse matrices
float4x4 matInvW;

float4x4 matShadowVP; //Shadow Camera View Projection
float4x4 matShadowVPP;//Shadow Camera View Projection Projected

//Camera
uniform float3  cCamPos;
uniform float3  cCamDir;

//Settings
uniform float4  cTime; //Time, 0-1 LoopedTime, ElapsedTime, 0.0

//Light
uniform float3 cLightPos;
uniform float3 cLightDiffuse;
uniform float3 cLightAmbient;
uniform float3 cLightAttenuation;


////////////////////////////////////////////////////////////////////
//Textures and Samplers/////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

uniform texture Texture0;
uniform texture Texture1;
uniform texture Texture2;
uniform texture Texture3;
uniform texture Texture4;
uniform texture Texture5;
uniform texture Texture6;
uniform texture Texture7;
uniform texture Texture8;
uniform texture Texture9;
uniform texture Texture10;
uniform texture Texture11;
uniform texture Texture12;
uniform texture Texture13;
uniform texture Texture14;
uniform texture Texture15;

//Per Name
uniform sampler cSamplerTex0 = sampler_state{ Texture = <Texture0>;};
uniform sampler cSamplerTex1 = sampler_state{ Texture = <Texture1>; };
uniform sampler cSamplerTex2 = sampler_state{ Texture = <Texture2>; };
uniform sampler cSamplerTex3 = sampler_state{ Texture = <Texture3>; };
uniform sampler cSamplerTex4 = sampler_state{ Texture = <Texture4>; };
uniform sampler cSamplerTex5 = sampler_state{ Texture = <Texture5>; };
uniform sampler cSamplerTex6 = sampler_state{ Texture = <Texture6>; };
uniform sampler cSamplerTex7 = sampler_state{ Texture = <Texture7>; };
uniform sampler cSamplerTex8 = sampler_state{ Texture = <Texture8>; };
uniform sampler cSamplerTex9 = sampler_state{ Texture = <Texture9>; };
uniform sampler cSamplerTex10 = sampler_state{ Texture = <Texture10>; };
uniform sampler cSamplerTex11 = sampler_state{ Texture = <Texture11>; };
uniform sampler cSamplerTex12 = sampler_state{ Texture = <Texture12>; };
uniform sampler cSamplerTex13 = sampler_state{ Texture = <Texture13>; };
uniform sampler cSamplerTex14 = sampler_state{ Texture = <Texture14>; };
uniform sampler cSamplerTex15 = sampler_state{ Texture = <Texture15>; };