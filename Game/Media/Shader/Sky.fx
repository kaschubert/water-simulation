#include "Constants.fx"

////////////////////////////////////////////////////////////////
//Technique 0: Object 2 Framebuffer/////////////////////////////
////////////////////////////////////////////////////////////////

void vs(in float3 iPosition0  : POSITION0,
		in float3 iNormal0    : NORMAL0,
		in float2 iTexCoord0  : TEXCOORD0,
		in float3 iBiTangent0 : BINORMAL0,		
		in float3 iTangent0   : TANGENT0,

		out float4 oPosition  : POSITION0,
		out float3 oTexCoord  : TEXCOORD0)
{
	float3 WorldPos = mul(float4(iPosition0, 1.0), matVP);
	
    oPosition = mul(float4(WorldPos + cCamPos, 1.0), matVP);
    
    oTexCoord = WorldPos;
}

void ps(in float3 iTexCoord : TEXCOORD0,
		  
		out float4 oColor0 : COLOR0,
		out float4 oColor1 : COLOR1)
{	
	//Sample sky cube texture
	//oColor.rgba = texCUBE(cSamplerTex2, iTexCoord);

	float3 CubeMap = texCUBE(cSamplerTex2, iTexCoord);

	float Luminance = dot(CubeMap, 0.33333);

	oColor0.rgb = CubeMap;// * pow(Luminance, 2) * 2;
	oColor0.a   = 1.0;
	oColor1     = oColor0;
	oColor1.a	= 1000.0;
}

////////////////////////////////////////////////////////////////
//Techniques////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

//Description of the vertex declaration
string VertexDeclaration = "P3N3T2B3Z3";

//Default
technique t0
{
    pass p0
    {CULLMODE = NONE;
        Vertexshader = compile vs_2_0 vs();
        Pixelshader  = compile ps_2_0 ps();
    }
}

//No shadow technique