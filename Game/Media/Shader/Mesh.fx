#include "Constants.fx"

////////////////////////////////////////////////////////////////
//Technique 0: Object 2 Framebuffer/////////////////////////////
////////////////////////////////////////////////////////////////

void vs(in float3 iPosition0  			: POSITION0,
		in float3 iNormal0    			: NORMAL0,
		in float2 iTexCoord0  			: TEXCOORD0,
		in float3 iBiTangent0 			: BINORMAL0,		
		in float3 iTangent0   			: TANGENT0,
		
		out float4 oPosition   			: POSITION0,
		out float2 oTexCoord0  			: TEXCOORD0,
		out float3 oNormalWS   			: TEXCOORD1, 
		out float3 oPositionWS 			: TEXCOORD2, 
		out float  oDistFromCamWS		: TEXCOORD3, 
		out float4 oShadowmapSS			: TEXCOORD4
		)
{
	float4 positionWS = mul(float4(iPosition0, 1.0f), matW);
	//Per Pixel Lighting
	oPositionWS = positionWS;
	oNormalWS = normalize(mul(iNormal0, matN));
	//For second render target alpha channel
	oDistFromCamWS = length(cCamPos.xyz - positionWS);
	
	//For Shadow mapping SS = ShadowSpace
	oShadowmapSS = mul(positionWS, matShadowVPP);
	
	//For pipeline
    oPosition = mul(positionWS, matVP);
	oTexCoord0 = iTexCoord0;
} 

void ps(in float2  iTexCoord0     		: TEXCOORD0,
		in float3  iNormalWS      		: TEXCOORD1,
		in float3  iPositionWS    		: TEXCOORD2, 
		in float   iDistFromCamWS 		: TEXCOORD3,
		in float4  iShadowmapSS			: TEXCOORD4,
		out float4 oColor0				: COLOR0, 
		out float4 oColor1				: COLOR1
		)
{
	float3 LightDirWS = normalize(iPositionWS - cLightPos);
	float3 Vert2CamWS = normalize(cCamPos - iPositionWS);

	float3 ReflectWS = normalize(reflect(LightDirWS, iNormalWS));
	float specular = 0.0;
	//Just add specular to parts of the mesh, that could be wet
	if(iPositionWS.y < 1.5)
		specular = pow(saturate(dot(ReflectWS, Vert2CamWS)), 128);
    float diffuse = saturate(dot(-LightDirWS, iNormalWS));
	float4 texel = tex2D(cSamplerTex0, iTexCoord0);
	
	//Shadow Mapping		
	
	float4 shadowmapTS = mul(iShadowmapSS, matT);
	
	float shadowDepthSS = tex2D(cSamplerTex1, shadowmapTS.xy).x;
	
	iShadowmapSS.z /= iShadowmapSS.w;
	
	if ((shadowDepthSS + 0.005) < iShadowmapSS.z)
	{
		// we're in shadow, cut the diffuse & specular light
		//diffuse = 0;
		//specular = 0;
	};
	
	oColor0.rgb = texel.rgb * (cLightAmbient.rgb + diffuse * cLightDiffuse.rgb);// + specular;
	oColor0.a   = texel.a;
	
	//second render target
	oColor1.rgb = oColor0.rgb;
	oColor1.a = iDistFromCamWS;
}

////////////////////////////////////////////////////////////////
//Technique 1: Object 2 Shadowmap///////////////////////////////
////////////////////////////////////////////////////////////////

void vsShadow(in float3 iPosition0  : POSITION0,
			  in float3 iNormal0    : NORMAL0,
			  in float2 iTexCoord0  : TEXCOORD0,
		 
			  out float4 oPosition  : POSITION0,
			  out float2 oTexCoord0 : TEXCOORD0,
			  out float4 oPosition1 : TEXCOORD1)
{
	//WorldMatrix
	float3 WorldPos = mul(float4(iPosition0, 1.0), matW);
	
	//Transform into Shadowspace
    oPosition  = mul(float4(WorldPos, 1.0), matShadowVP);
    oPosition1 = oPosition;

    oTexCoord0 = iTexCoord0;
}

void psShadow(in float2 iTexCoord0 : TEXCOORD0, 
			  in float4 iPosition  : TEXCOORD1,
		 
			  out float4 oColor    : COLOR0)
{	
	oColor = (iPosition.z / iPosition.w) - 0.1;	 
}

////////////////////////////////////////////////////////////////
//Techniques////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

//Description of the vertex declaration
string VertexDeclaration = "P3N3T2";

//Default
technique t0
{
    pass p0
    {
        Vertexshader = compile vs_2_0 vs();
        Pixelshader  = compile ps_2_0 ps();
    }
}

//Shadow
technique t1
{
    pass p0
    {		
        Vertexshader = compile vs_2_0 vsShadow();
        Pixelshader  = compile ps_2_0 psShadow();
    }
}