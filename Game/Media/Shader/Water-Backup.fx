#include "Constants.fx"

////////////////////////////////////////////////////////////////

//Input from the pipeline for the vertexshader
struct VS_INPUT
{
	float2 Position : POSITION0;
};

//Output of the vertexshader
struct VS_OUTPUT
{
	float4 Position     : POSITION0;
	float4 TexCoord     : TEXCOORD0;
	float3 NormalTAS     : TEXCOORD1;
	float3 NormalWS     : TEXCOORD2;
	float3 LightDirWS   : TEXCOORD3;
	float3 LightDirTAS  : TEXCOORD4;
	float4 CamDirWS     : TEXCOORD5;
	float4 CamDirTAS    : TEXCOORD6;	
	float4 PositionWS   : TEXCOORD7;
};

//Output of the pixelshader
struct PS_OUTPUT
{
	float4 Color  : COLOR0;
};
//World space 2 tangent space
float3x3 matTAS = 0;
float3x3 matTASInv = 0;
//number of waves
int nWaves = 3;

//contains wave data
struct WAVE
{
	float amplitude;
	float speed;
	float frequency;
	float2 dir;
	float k;
	float q;
} WAVES[3];

////////////////////////////////////////////////////////////////
//Functions/////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

//Compute Gerstnerwavegeometry and normals
void computeGerstnerWaves(inout float3 PositionWS, inout float3 normal, inout float3 binormal, inout float3 tangent)
{
	float3 wavePos;
	normal = 0.f;
	binormal = 0.f;
	tangent = 0.f;
	
	//Geometry
	for(int i = 0; i < nWaves; i++)
	{
		float Qi = saturate(WAVES[i].q / (WAVES[i].frequency * WAVES[i].amplitude * nWaves));
		float distance2Plane = dot(WAVES[i].dir, PositionWS.xz);
		float sinCosTerm = WAVES[i].frequency * distance2Plane + cTime.x * WAVES[i].speed;
		float xzTerm = Qi * WAVES[i].amplitude * cos(sinCosTerm);
		
		wavePos.x += xzTerm * WAVES[i].dir.x;
		wavePos.z += xzTerm * WAVES[i].dir.y; //y equals z coord here
		wavePos.y += WAVES[i].amplitude * sin(sinCosTerm);
	}
	wavePos.x += PositionWS.x;
	wavePos.z += PositionWS.z;
	
	PositionWS = wavePos;
	
	//Normals
	for(int i = 0; i < nWaves; i++)
	{
		float WA = WAVES[i].frequency * WAVES[i].amplitude;
		float DOT = dot(WAVES[i].dir, wavePos.xz);
		float SINCOS = WAVES[i].frequency * DOT + cTime.x * WAVES[i].speed;
		float SIN = sin(SINCOS);
		float COS = cos(SINCOS);
		float Qi = saturate(WAVES[i].q / (WAVES[i].frequency * WAVES[i].amplitude * nWaves));
		
		normal.x += WAVES[i].dir.x * WA * COS;
		normal.y += Qi * WA * SIN;
		normal.z += WAVES[i].dir.y * WA * COS;
		
		tangent.x += Qi * WAVES[i].dir.x * WAVES[i].dir.y * WA * SIN;
		tangent.y += WAVES[i].dir.y * WA * COS;
		tangent.z += Qi * pow(WAVES[i].dir.y, 2) * WA * SIN;
		
		binormal.x += Qi * pow(WAVES[i].dir.x, 2) * WA * SIN;
		binormal.y += WAVES[i].dir.y * WA * COS;
		binormal.z += Qi * WAVES[i].dir.x * WAVES[i].dir.y * WA * SIN;
		
	}
	normal.x = - normal.x;
	normal.y = 1 - normal.y;
	normal.z = - normal.z;
	
	tangent.x = - tangent.x;
	tangent.z = 1 - tangent.z;
	
	binormal.x = 1 - binormal.x;
	binormal.z = - binormal.z;
}

//Compute Height of single Wave and derive in x/z-Direction for single wave
float computeDeriveW(in int nWave, in float2 PositionWSXZ, inout float2 derive)
{
	//Common used terms
	float distance2Plane = dot(WAVES[nWave].dir, PositionWSXZ);
	float sinCosTerm = distance2Plane * WAVES[nWave].frequency + cTime.x * WAVES[nWave].speed;
	float kTerm = (sin(sinCosTerm) + 1) * 0.5;
	
	//Derivates
	float powKminus1 = pow(kTerm , WAVES[nWave].k - 1);
	
	float deriveXorZ =	WAVES[nWave].k * WAVES[nWave].frequency * WAVES[nWave].amplitude
						* powKminus1
						* cos(sinCosTerm);
	
	derive.x = deriveXorZ * WAVES[nWave].dir.x;
	derive.y = deriveXorZ * WAVES[nWave].dir.y; //dir.y meaning dir.z saved in second slot of vector2 dir
	
	//calculate Height
	return 2 * WAVES[nWave].amplitude * powKminus1*WAVES[nWave].k - 2;
}

//Add all single Waves for accumulated result and add derivates for Normal
float2 computeDeriveH(in float2 PositionWSXZ, out float height)
{
	height = 0.0;
	float2 derive = 0.0; 
	
	for(int i = 0; i < nWaves; i++)
	{
		float2 derivetemp = float2(0.0, 0.0);
		
		height += computeDeriveW(i, PositionWSXZ, derivetemp);
		
		derive += derivetemp;
	}
	//Minus because of negativ derivates, but positive tangent and binormal
	derive = -derive;
	
	return derive;
}

//Compute how much light is reflected and how much is refracted
float fresnel(float3 light, float3 normal, float R0)
{
	// Note: compute R0 on the CPU and provide as a
	// constant; it is more efficient than computing R0 in
	// the vertex shader. R0 is:
	// float const R0 = pow(1.0-refractionIndexRatio, 2.0)
	//					pow(1.0+refractionIndexRatio, 2.0);
	// light and normal are assumed to be normalized
	return R0 + (1.0-R0) * pow(1.0-dot(light, normal), 5.0);
}

void initWaves()
{
	WAVES[0].amplitude = 2.0;
	WAVES[0].speed = 0.7;
	WAVES[0].frequency = 0.1;
	WAVES[0].dir = float2(0,1);
	WAVES[0].k = 1.0;
	WAVES[0].q = 1;
	
	WAVES[1].amplitude = 0.5;
	WAVES[1].speed = 2.0;
	WAVES[1].frequency = 0.3;
	WAVES[1].dir = float2(0.5,0.5);
	WAVES[1].k = 1.5;
	WAVES[1].q = 1;
	
	WAVES[2].amplitude = 0.3;
	WAVES[2].speed = 5.5;
	WAVES[2].frequency = 0.7;
	WAVES[2].dir = float2(-0.3,0.7);
	WAVES[2].k = 3;
	WAVES[2].q = 1;
}

float3x3 inverseMatrix(float3x3 mat)
{
	float3 temp = 0;
	float3x3 returnMat = {1.f,0.f,0.f,
					   0.f,1.f,0.f,
					   0.f,0.f,1.f};
	temp = returnMat[0];
	returnMat[0] = returnMat[2];
	returnMat[2] = temp;
	
	returnMat[1] -= 2 * returnMat[0];
	returnMat[2] -= 3 * returnMat[0];
	
	temp = returnMat[1];
	returnMat[1] = returnMat[2];
	returnMat[2] = temp;
	
	returnMat[1] *= -1;
	
	returnMat[0] -= 2* returnMat[1];
	returnMat[0] += 8* returnMat[2];
	
	returnMat[1] -= 5* returnMat[2];
	
	return returnMat;
}

////////////////////////////////////////////////////////////////
//Shaders///////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

void vs(in VS_INPUT In, out VS_OUTPUT Out)
{
	//Hardcoded size
	float Size = 138.0;
	//set data for waves
	initWaves();
		
	//Set Grid
	Out.PositionWS = float4(In.Position.x * Size - 72, -3.0, In.Position.y * Size - 64, 1.0);
	
	//Tangent Space Vectors
	float3 tempNormal = float3(0.0, 1.0, 0.0);
	float3 tempBinormal = float3(1.0, 0.0, 0.0);
	float3 tempTangent = float3(0.0, 0.0, 1.0);
	//Tangent Space Transform
	matTAS[0] = normalize(tempBinormal);
	matTAS[1] = normalize(tempTangent);
	matTAS[2] = normalize(tempNormal);
	
	matTASInv = inverseMatrix(matTAS);
	
	computeGerstnerWaves(Out.PositionWS.xyz, tempNormal, tempBinormal, tempTangent);
	/*float height;
	tempNormal.xz = computeDeriveH(PositionWS.xz, height);
	PositionWS.y = height;//*/
	
	Out.NormalWS = normalize(tempNormal);
	Out.NormalTAS = mul(Out.NormalWS, matTAS);
	Out.LightDirWS = normalize(Out.PositionWS.xyz - cLightPos);
	Out.LightDirTAS = mul(Out.LightDirWS, matTAS);
	Out.CamDirWS.w   = length(Out.PositionWS.xyz - cCamPos.xyz);
	Out.CamDirWS.xyz = float4(normalize(Out.PositionWS.xyz - cCamPos.xyz), 1.0);
	Out.CamDirTAS = float4(mul(Out.CamDirWS.xyz, matTAS), 1.0);	
	
	Out.Position = mul(Out.PositionWS, matVP);
	
	//TODO move to CPU
	float4x4 matTT = {	0.5f,  0.0f,  0.0f,  0.0f,
						0.0f, -0.5f,  0.0f,  0.0f,
						0.0f,  0.0f,  0.5f,  0.0f,
						0.5f,  0.5f,  0.5f,  1.0f
	};
	
	//Faking water wobble
	float4 PositionTex;
	float R0 = pow(0.5/*1.000293 - 1.333333*/, 2) / pow(2.5/*1.000293 + 1.333333*/, 2);
	
	PositionTex.xz = Out.PositionWS.xz + Out.NormalWS.xz * 4.0f;//+ refract(Out.NormalWS, Out.CamDirWS, R0).xz * 8.0f;//*/
	PositionTex.y = Out.PositionWS.y;
	PositionTex.w = 1.0;
	float4 PositionTexPS = mul(PositionTex, matVP);//*/
	
	//Map to Texture Space
	Out.TexCoord = mul(PositionTexPS, matTT);
}

////////////////////////////////////////////////////////////////

void ps(in VS_OUTPUT In, out PS_OUTPUT Out)
{	
//Lighting from calculated Normals
	//Reflect for specular
	float3 ReflectWS = normalize(reflect(In.LightDirWS, In.NormalWS));
	//For Cube mapping
	float3 ReflectViewWS = (reflect(In.CamDirWS, In.NormalWS));
	
	//Lighting parts
    float specular = pow(saturate(dot(ReflectWS, -In.CamDirWS)), 64);
    float diffuse = saturate(dot(-In.LightDirWS, In.NormalWS));
//Lighting from Normal Map
	//Calc normal from [-1, 1] to [0, 1] and stay at |normal| == 1
    float3 normalFromMap = tex2D(cSamplerTex2, In.PositionWS.xz * 0.02) * 2.0f - 1.0f;
    normalFromMap = normalize(normalFromMap + In.NormalTAS);
    //Reflect for specular
	float3 ReflectTAS = normalize(reflect(In.LightDirTAS.xyz, normalFromMap));
	//For Cube mapping
	float3 ReflectViewTAS = (reflect(In.CamDirTAS.xyz, normalFromMap));
	
	//Lighting parts
    float specularTAS = pow(saturate(dot(ReflectTAS, -In.CamDirTAS.xyz)), 64);
    float diffuseTAS = saturate(dot(-In.LightDirTAS.xyz, normalFromMap));
//Foam
	float3 normalFromMapWS = mul(normalFromMap, matTASInv);
	float foamBlend = 0;
	for(int i = 0; i < nWaves; i++)
	{
		if(dot(float2(0,1), In.NormalWS.xz) > 0.7)
			foamBlend += 0.3;//WAVES[i].amplitude;
	}
	//foamBlend = saturate(foamBlend);
	float3 FoamMap = tex2D(cSamplerTex3, In.PositionWS.xz * 0.1) * foamBlend;
	
	
    //TODO move to CPU
    float R0 = pow(0.5/*1.000293 - 1.333333*/, 2) / pow(2.5/*1.000293 + 1.333333*/, 2);
    
    //Get ground texture
    float4 meshColor = tex2Dproj(cSamplerTex0, In.TexCoord);
    
	float3 reflectColor = texCUBE(cSamplerTex1, ReflectViewWS);
	float3 refractColor = meshColor.rgb;
	
	//Fake
	reflectColor += specularTAS * 60;
	
	//Calc distance from waterline to seaground
	float waterDeepWS = meshColor.a - In.CamDirWS.w;
	
	waterDeepWS = saturate(waterDeepWS/55.0);
	float3 waterColor = float3(0, 1, 2) + FoamMap;
	
	//Fake Scattering
	refractColor = lerp(refractColor * pow(1.0 - waterDeepWS, float3(0.125, 1.3, 1)), waterColor, waterDeepWS);
	//*/
	//fresnel
    float fresnel = saturate(fresnel(-In.CamDirWS, In.NormalWS, R0));//*/
	
	Out.Color.rgb = lerp(refractColor, reflectColor, fresnel);// * (cLightAmbient + diffuseTAS * cLightDiffuse + specularTAS); 
	Out.Color.a = 1.0;
	//Debug		
	//Out.Color = float4(In.NormalWS.xyz, 1.0);
}

////////////////////////////////////////////////////////////////
//Techniques////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

//Description of the vertex declaration
string VertexDeclaration = "P2";

//Default
technique t0
{
    pass p0
    {
		MAGFILTER[0] = LINEAR;
		MAGFILTER[1] = LINEAR;
		MAGFILTER[2] = LINEAR;
		MAGFILTER[3] = LINEAR;
		MINFILTER[0] = LINEAR;
		MINFILTER[1] = LINEAR;
		MINFILTER[2] = LINEAR;
		MINFILTER[3] = LINEAR;

		ADDRESSU[0] = MIRROR;
		ADDRESSV[0] = MIRROR;

		//CULLMODE		 = NONE;
		//FILLMODE		 = WIREFRAME;
		//ALPHABLENDENABLE = TRUE;
		//SRCBLEND		 = SRCALPHA;
		//DESTBLEND		 = INVSRCALPHA;

        Vertexshader = compile vs_3_0 vs();
        Pixelshader  = compile ps_3_0 ps();
    }
}